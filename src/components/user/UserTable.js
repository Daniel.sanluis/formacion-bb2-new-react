import React from "react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { Edit, Delete } from "@material-ui/icons";
import Fab from "@material-ui/core/Fab";
import Select from "@material-ui/core/Select";
import NativeSelect from "@material-ui/core/NativeSelect";
import Switch from "@material-ui/core/Switch";
import InputLabel from "@material-ui/core/InputLabel";
import AddIcon from "@material-ui/icons/Add";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import IconButton from "@material-ui/core/IconButton";
import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Modal,
  Button,
  TextField,
  useTheme,
} from "@material-ui/core";
import swal from "sweetalert";
import { getUsers } from "../../services/user/getUsers";
import { getRoles } from "../../services/role/getRoles";
import { saveUser } from "../../services/user/saveUser";
import { deleteUser } from "../../services/user/deleteUser";
import { updateUser } from "../../services/user/updateUser";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },

  margin: {
    margin: theme.spacing(1),
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "100%",
  },
}));

export function UserTable({ history }) {
  const [modalSave, setModalSave] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const styles = useStyles();
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users.users);
  const roles = useSelector((state) => state.roles.roles);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [flagState, setFlagState] = useState(false);

  const [roleSelected, setRoleSelected] = useState({
    idRole: "",
    role: "",
    description: "",
  });

  const [userSelected, setUserSelected] = useState({
    idUser: "",
    username: "",
    password: "",
    token: null,
    enabled: "",
    roles: roleSelected,
  });

  const useStyles1 = makeStyles((theme) => ({
    root: {
      flexShrink: 0,
      marginLeft: theme.spacing(2.5),
    },
  }));

  useEffect(() => {
    getUsers(dispatch);
    getRoles(dispatch);
  }, [flagState]);

  function TablePaginationActions(props) {
    const classes = useStyles1();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage } = props;

    const handleFirstPageButtonClick = (event) => {
      onChangePage(event, 0);
    };

    const handleBackButtonClick = (event) => {
      onChangePage(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
      onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
      onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
      <div className={classes.root}>
        <IconButton
          onClick={handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="first page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={handleBackButtonClick}
          disabled={page === 0}
          aria-label="previous page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
              <KeyboardArrowLeft />
            )}
        </IconButton>
        <IconButton
          onClick={handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="next page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
              <KeyboardArrowRight />
            )}
        </IconButton>
        <IconButton
          onClick={handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="last page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, users?.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserSelected((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleChangeMultiple = (event) => {
    const { options } = event.target;
    const value = [];
    for (let i = 0, l = options.length; i < l; i += 1) {
      if (options[i].selected) {
        value.push(JSON.parse(options[i].value));
      }
    }
    setUserSelected((prevState) => ({
      ...prevState,
      roles: value,
    }));
  };

  const selectUser = (user, mode) => {
    setUserSelected(user);
    mode === "Edit" ? setModalEdit(true) : modalOpenCloseDelete(true);
  };

  const checkEnable = (value) => {
    if (value) {
      return (
        <Switch

          checked
          color="primary"
          inputProps={{ 'aria-label': 'primary checkbox' }}
        />
      );
    } else {
      return (
        <Switch
          disabled
          color="primary"
          inputProps={{ "aria-label": "primary checkbox" }}
        />
      );
    }
  };

  const handleSave = () => {
    saveUser(dispatch, userSelected);
    setFlagState(true);
    modalOpenCloseSave();
  };

  const handleUpdate = () => {
    if (userSelected.idUser == localStorage.getItem('idUser')) {
      swal("Error!", "You cannot update the active user", "error");
    } else {
      updateUser(dispatch, userSelected);
      setFlagState(true);
      //  window.location.reload();
      modalOpenCloseEdit();
    }
  }

  const handleDelete = () => {
    if (userSelected.idUser == localStorage.getItem('idUser')) {
      swal("Error!", "You cannot remove the active user", "error");
    } else {
      deleteUser(dispatch, userSelected.idUser);
      setFlagState(true);
      modalOpenCloseDelete();
    }
  };

  const modalOpenCloseSave = () => {
    setModalSave(!modalSave);
  };

  const modalOpenCloseEdit = () => {
    setModalEdit(!modalEdit);
  };

  const modalOpenCloseDelete = () => {
    setModalDelete(!modalDelete);
  };

  const bodyModalSave = (
    <form onSubmit={() => handleSave()}>
      <div className={styles.modal}>
        <h3>Add new User</h3>
        <br />
        <TextField
          name="username"
          required
          className={styles.inputMaterial}
          label="Username"
          onChange={handleChange}
        />
        <br />
        <TextField
          name="password"
          type="password"
          required
          className={styles.inputMaterial}
          label="Password"
          onChange={handleChange}
        />

        <InputLabel className="mt-3">Select Suppliers</InputLabel>

        <Select
          multiple
          native
          required
          name="roles"
          className={styles.inputMaterial}
          label="Roles"
          onChange={handleChangeMultiple}
          inputProps={{
            id: "select-multiple-native",
          }}
        >
          {roles &&
            roles.map((role) => (
              <option key={role.idRole} value={JSON.stringify(role)}>
                {role.role}
              </option>
            ))}
        </Select>
        <br />
        <InputLabel className="mt-3" >Enabled</InputLabel>
        <NativeSelect
          required
          name="enabled"
          className={styles.inputMaterial}
          label="Enabled"
          onChange={handleChange}>
          <option value='true'>TRUE</option>
          <option value='false'>FALSE</option>
        </NativeSelect>
        <br />
        <div align="right">
          <Button type="submit" color="primary">
            Save
          </Button>
          <Button onClick={modalOpenCloseSave}>Cancel</Button>
        </div>
      </div>
    </form>
  );

  const bodyModalEdit = (

    <form onSubmit={() => handleUpdate()}>

      <div className={styles.modal}>
        <h3>Update User</h3>
        <br />
        <TextField
          name="username"
          required
          className={styles.inputMaterial}
          label="Username"
          onChange={handleChange}
          value={userSelected && userSelected.username}
        />
        <br />
        <TextField
          name="password"
          type="password"
          required
          className={styles.inputMaterial}
          label="Password"
          onChange={handleChange}
          value={userSelected && userSelected.password}
        />
        <InputLabel className="mt-3">Select Roles</InputLabel>
        <Select
          multiple
          native
          required
          name="roles"
          className={styles.inputMaterial}
          label="Roles"
          onChange={handleChangeMultiple}
          inputProps={{
            id: "select-multiple-native",
          }}
        >
          {roles &&
            roles.map((role) => (
              <option key={role.idRole} value={JSON.stringify(role)}>
                {role.role}
              </option>
            ))}
        </Select>
        <br />
        <InputLabel className="mt-3" >Enabled</InputLabel>
        <NativeSelect
          required
          name="enabled"
          className={styles.inputMaterial}
          label="Enabled"
          value={userSelected && userSelected.enabled}
          onChange={handleChange}>
          <option value='true'>TRUE</option>
          <option value='false'>FALSE</option>
        </NativeSelect>
        <br />
        <div align="right">
          <Button type="submit" color="primary">
            Edit
        </Button>
          <Button onClick={modalOpenCloseEdit}>Cancel</Button>
        </div>
      </div>
    </form>
  );

  const bodyModalDelete = (
    <div className={styles.modal}>
      <p>
        Are you sure you want to delete the user:{" "}
        <b>{userSelected && userSelected.username}</b> ?{" "}
      </p>
      <div align="right">
        <Button color="secondary" onClick={() => handleDelete()}>
          Yes
        </Button>
        <Button onClick={() => modalOpenCloseDelete()}>No</Button>
      </div>
    </div>
  );


  if (users  && users !== null && users !== undefined) {
    return (
      <>
        <div className="text-center">
          <Fab onClick={() => modalOpenCloseSave()} color="primary" aria-label="add">
            <AddIcon
              className={styles.iconos}
            />
          </Fab>
          <br />
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell style={{ fontWeight: "bold" }}>#ID</TableCell>
                  <TableCell style={{ fontWeight: "bold" }}>USERNAME</TableCell>
                  <TableCell style={{ fontWeight: "bold" }}>ROLES</TableCell>
                  <TableCell style={{ fontWeight: "bold" }}>PASSWORD</TableCell>
                  <TableCell style={{ fontWeight: "bold" }}>ENABLED</TableCell>
                  <TableCell style={{ fontWeight: "bold" }}>ACTIONS</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {(rowsPerPage > 0 //TODO
                  ? users &&
                  users.slice(
                    page * rowsPerPage,
                    page * rowsPerPage + rowsPerPage
                  )
                  : users && users
                ).map((user) => (
                  <TableRow key={user.idUser}>
                    <TableCell>#{user.idUser}</TableCell>
                    <TableCell>{user.username}</TableCell>
                    <TableCell>
                      {user.roles &&
                        user.roles.map((role, index) => {
                          return role.role + " ";
                        })}
                    </TableCell>
                    <TableCell>{user.password}</TableCell>
                    <TableCell>
                      {checkEnable(user.enabled)}
                    </TableCell>
                    <TableCell>
                      <Edit onClick={() => selectUser(user, 'Edit')} className={styles.iconos} />
                      &nbsp;&nbsp;&nbsp;
                      <Delete
                        onClick={() => selectUser(user, "Delete")}
                        color="secondary"
                        className={styles.iconos}
                      />
                    </TableCell>
                  </TableRow>
                ))}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
                    colSpan={7}
                    count={users?.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: { "aria-label": "rows per page" },
                      native: true,
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>

          <Modal open={modalSave} onClose={modalOpenCloseSave}>
            {bodyModalSave}
          </Modal>

          <Modal open={modalEdit} onClose={modalOpenCloseSave}>
            {bodyModalEdit}
          </Modal>

          <Modal open={modalDelete} onClose={modalOpenCloseDelete}>
            {bodyModalDelete}
          </Modal>
        </div>
      </>
    );
  } else {
    return null;
  }
};
