import React from "react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { makeStyles } from "@material-ui/core/styles";
import { Edit, Delete, Visibility } from "@material-ui/icons";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Select from "@material-ui/core/Select";
import NativeSelect from "@material-ui/core/NativeSelect";
import InputLabel from "@material-ui/core/InputLabel";
import Grid from "@material-ui/core/Grid";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import IconButton from "@material-ui/core/IconButton";
import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Modal,
  Button,
  TextField,
  useTheme,
} from "@material-ui/core";
import swal from "sweetalert";
import { getItems } from "../../services/item/getItems";
import { deleteItem } from "../../services/item/deleteItem";
import { saveItem } from "../../services/item/saveItem";
import { updateItem } from "../../services/item/updateItem";
import { getSuppliers } from "../../services/supplier/getSuppliers";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },

  margin: {
    margin: theme.spacing(1),
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "100%",
  },
}));

export function ItemTable({ history }) {
  const [modalSave, setModalSave] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [modalPriceReduction, setModalPriceReduction] = useState(false);

  const [flagState, setFlagState] = useState(false);


  const [priceReductionSelected, setPriceReductionSelected] = useState({
    idPriceReduction: "",
    reducedPrice: "",
    startDate: "",
    endDate: "",
  });

  const [supplierSelected, setSupplierSelected] = useState({
    idSupplier: "",
    name: "",
    country: "",
  });

  const [itemSelected, setItemSelected] = useState({
    idItem: "",
    itemCode: "",
    description: "",
    creationDate: "",
    price: "",
    priceReductions: [],
    suppliers: supplierSelected,
    messageDisabled: "",
    state: "",

  });

  const styles = useStyles();

  const dispatch = useDispatch();
  useEffect(() => {    
    getItems(dispatch);   
    getSuppliers(dispatch);
  }, [flagState]);

  const items = useSelector((state) => state.items.items);

  const suppliers = useSelector((state) => state.suppliers.suppliers);

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const useStyles1 = makeStyles((theme) => ({
    root: {
      flexShrink: 0,
      marginLeft: theme.spacing(2.5),
    },
  }));

  function checkRoleActions(item) {
    const currentRoles = JSON.parse(localStorage.getItem('role'));
    const roleAdmin = currentRoles.find(role => role.role == "ROLE_ADMIN");
    if (roleAdmin && roleAdmin.role === "ROLE_ADMIN") {
      return (
        <TableCell>
          <Visibility
            onClick={() => handleItemDetail(item.idItem)}
            color="primary"
            className={styles.iconos}
          />
                    &nbsp;&nbsp;&nbsp;
          <Edit
            onClick={() => selectItem(item, "Edit")}
            className={styles.iconos}
          />
                    &nbsp;&nbsp;&nbsp;
          <Delete
            onClick={() => selectItem(item, "Delete")}
            color="secondary"
            className={styles.iconos}
          />
        </TableCell>
      );
    } else {
      return (
        <TableCell className="text-center">
          <Visibility
            onClick={() => handleItemDetail(item.idItem)}
            color="primary"
            className={styles.iconos}
          />

        </TableCell>
      );
    }
  }

  function checkRoleAdd() {
    const currentRoles = JSON.parse(localStorage.getItem('role'));
    const roleAdmin = currentRoles.find(role => role.role == "ROLE_ADMIN");
    if (roleAdmin && roleAdmin.role === "ROLE_ADMIN") {
      return (
        <Fab onClick={() => modalOpenCloseSave()} color="primary" aria-label="add">
          <AddIcon
            className={styles.iconos}
          />
        </Fab>
      );
    }
  }

  function messageDiscontuned(value, mode) {
    if (value == "DISCONTINUED") {
      if (mode == "Save") {
        return (
          <TextField
            name="messageDisabled"
            required
            className={styles.inputMaterial}
            label="Reason"
            onChange={handleChange}
          />
        );
      } else {
        return (
          <TextField
            name="messageDisabled"
            required
            className={styles.inputMaterial}
            label="Reason"
            onChange={handleChange}
            value={itemSelected && itemSelected.messageDisabled}
          />
        );
      }
    }
  }

  function TablePaginationActions(props) {
    const classes = useStyles1();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage } = props;

    const handleFirstPageButtonClick = (event) => {
      onChangePage(event, 0);
    };

    const handleBackButtonClick = (event) => {
      onChangePage(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
      onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
      onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
      <div className={classes.root}>
        <IconButton
          onClick={handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="first page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={handleBackButtonClick}
          disabled={page === 0}
          aria-label="previous page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
              <KeyboardArrowLeft />
            )}
        </IconButton>
        <IconButton
          onClick={handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="next page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
              <KeyboardArrowRight />
            )}
        </IconButton>
        <IconButton
          onClick={handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="last page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, items?.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };



  const handleItemDetail = (id) => {
    history.push(`/itemDetails/${id}`);
  };

  const handleChangeMultiple = (event) => {
    const { options } = event.target;

    const value = [];
    for (let i = 0, l = options.length; i < l; i += 1) {
      if (options[i].selected) {
        value.push(JSON.parse(options[i].value));
      }
    }
    setItemSelected((prevState) => ({
      ...prevState,
      suppliers: value,
    }));
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setItemSelected((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleAddPriceReductions = () => {
    if (priceReductionSelected.startDate > priceReductionSelected.endDate) {
      swal("Error", "The start date cannot be less than today or greater than the end date", "error");
    } else {
      const lista = itemSelected.priceReductions;
      lista.push(priceReductionSelected);
      setItemSelected((prevState) => ({
        ...prevState,
        priceReductions: lista,
      }));
    }
  };

  const handleChangePriceReduction = (e) => {
    const { name, value } = e.target;
    if (name != "reducedPrice") {
      setPriceReductionSelected((prevState) => ({
        ...prevState,
        [name]: moment(value).format('DD/MM/YYYY')

      }));
    } else {
      setPriceReductionSelected((prevState) => ({
        ...prevState,
        [name]: value,

      }));
    }
  };



  const selectItem = (item, mode) => {
    setItemSelected(item);
    mode === "Edit" ? setModalEdit(true) : modalOpenCloseDelete(true);
  };

  const handleDelete = () => {
    deleteItem(dispatch,itemSelected.idItem);
    setFlagState(true);
    modalOpenCloseDelete();
  };

  const handleSave = () => {
    saveItem(dispatch,itemSelected);
    setFlagState(true);
    modalOpenCloseSave();
  };

  const handleUpdate = () => {
    updateItem(dispatch,itemSelected);
    setFlagState(true);
    modalOpenCloseEdit();
  };

  const modalOpenCloseSave = () => {
    setModalSave(!modalSave);
  };

  const modalOpenCloseEdit = () => {
    setModalEdit(!modalEdit);
  };

  const modalOpenCloseDelete = () => {
    setModalDelete(!modalDelete);
  };

  const modalOpenClosePriceReduction = () => {
    setModalPriceReduction(!modalPriceReduction);
  };

  const bodyModalSave = (
    <form onSubmit={() => handleSave()}>
      <div className={styles.modal}>
        <h3>Add new Item</h3>
        <br />
        <TextField
          name="itemCode"
          required
          type="number"
          className={styles.inputMaterial}
          label="Code"
          onChange={handleChange}
        />
        <br />
        <TextField
          name="description"
          required
          className={styles.inputMaterial}
          label="Description"
          onChange={handleChange}
        />
        <br />
        <TextField
          name="price"
          type="number"
          min="0"
          required
          className={styles.inputMaterial}
          label="Price"
          onChange={handleChange}
          inputProps={{
            step: "any",
          }}
        />
        <br />
        <InputLabel className="mt-3">Select Suppliers</InputLabel>
        <Select
          multiple
          native
          required
          name="suppliers"
          className={styles.inputMaterial}
          label="Suppliers"
          onChange={handleChangeMultiple}
          inputProps={{
            id: "select-multiple-native",
          }}
        >
          {suppliers &&
            suppliers.map((supplier) => (
              <option
                key={supplier.idSupplier}
                value={JSON.stringify(supplier)}
              >
                {supplier.name}
              </option>
            ))}
        </Select>
        <br />

        <InputLabel className="mt-3">State</InputLabel>
        <NativeSelect
          name="state"
          className={styles.inputMaterial}
          label="State"
          onChange={handleChange}
          defaultValue="ACTIVE"
        >
          <option value="ACTIVE">ACTIVE</option>
          <option value="DISCONTINUED">DISCONTINUED</option>
        </NativeSelect>
        <br />
        <br />
        {messageDiscontuned(itemSelected.state, 'Save')}
        <Grid container spacing={3}>
          <Grid item xs={9}>
            <InputLabel className="mt-3">
              <b>Add Price Reduction</b>
            </InputLabel>
          </Grid>
          <Grid item xs={3}>
            <Fab
              size="small"
              color="secondary"
              label="Price"
              aria-label="add"
              className={styles.margin}
            >
              <AddIcon
                aling="right"
                onClick={() => modalOpenClosePriceReduction()}
              />
            </Fab>
          </Grid>
        </Grid>
        <br />
        <div align="right">
          <Button type="submit" color="primary">
            Save
          </Button>
          <Button onClick={modalOpenCloseSave}>Cancel</Button>
        </div>
      </div>
    </form>
  );

  const bodyModalEdit = (
    <form onSubmit={() => handleUpdate()}>
      <div className={styles.modal}>
        <h3>Update Item</h3>

        <TextField
          name="description"
          className={styles.inputMaterial}
          label="Description"
          onChange={handleChange}
          value={itemSelected && itemSelected.description}
        />
        <br />

        <TextField
          name="price"
          type="number"
          step="any"
          className={styles.inputMaterial}
          label="Price"
          onChange={handleChange}
          value={itemSelected && itemSelected.price}
          inputProps={{
            step: "any",
          }}
        />
        <br />
        <TextField
          className={styles.inputMaterial}
          label="Price Reductions"
          onChange={handleChange}
        />
        <br />
        <Select
          multiple
          native
          name="suppliers"
          className={styles.inputMaterial}
          label="Suppliers"
          onChange={handleChangeMultiple}
          inputProps={{
            id: "select-multiple-native",
          }}
        >
          {suppliers &&
            suppliers.map((supplier) => (
              <option
                key={supplier.idSupplier}
                value={JSON.stringify(supplier)}
              >
                {supplier.name}
              </option>
            ))}
        </Select>
        <br />
        <br />
        <NativeSelect
          name="state"
          className={styles.inputMaterial}
          label="State"
          value={itemSelected && itemSelected.state}
          onChange={handleChange}
        >
          <option value="ACTIVE">ACTIVE</option>
          <option value="DISCONTINUED">DISCONTINUED</option>
        </NativeSelect>
        <br />
        {messageDiscontuned(itemSelected.state, 'Edit')}
        <Grid container spacing={3}>
          <Grid item xs={9}>
            <InputLabel className="mt-3">
              <b>Add Price Reduction</b>
            </InputLabel>
          </Grid>
          <Grid item xs={3}>
            <Fab
              size="small"
              color="secondary"
              label="Price"
              aria-label="add"
              className={styles.margin}
            >
              <AddIcon onClick={() => modalOpenClosePriceReduction()} />
            </Fab>
          </Grid>
        </Grid>

        <br />
        <div align="right">
          <Button type="submit" color="primary">
            Edit
          </Button>
          <Button onClick={modalOpenCloseEdit}>Cancel</Button>
        </div>
      </div>
    </form>
  );

  const bodyModalDelete = (
    <div className={styles.modal}>
      <p>
        Are you sure you want to delete the item:{" "}
        <b>{itemSelected && itemSelected.description}</b> ?{" "}
      </p>
      <div align="right">
        <Button color="secondary" onClick={() => handleDelete()}>
          Yes
        </Button>
        <Button onClick={() => modalOpenCloseDelete()}>No</Button>
      </div>
    </div>
  );

  const bodyModalPriceReduction = (
    <div className={styles.modal}>
      <h3>Add Price Reduction</h3>
      <TextField
        required
        type="number"
        name="reducedPrice"
        className={styles.inputMaterial}
        label="Price Reduction"
        onChange={handleChangePriceReduction}
      />
      <br />
      <InputLabel className="mt-3">
        Start Date
            </InputLabel>
      <TextField
        type="date"
        name="startDate"
        className={styles.inputMaterial}
        onChange={handleChangePriceReduction}
        inputProps={{
          min: moment().format("YYYY-MM-DD"),
          onkeydown: false
        }}
      />
      <br />
      <InputLabel className="mt-3">
        End Date
            </InputLabel>
      <TextField
        type="date"
        name="endDate"
        className={styles.inputMaterial}
        onChange={handleChangePriceReduction}
        inputProps={{
          min: moment(priceReductionSelected.startDate).add(1, 'days').format("YYYY-MM-DD"),
          onkeydown: false
        }}
      />
      <br />
      <br />
      <hr />
      <TableContainer>
        <InputLabel className="mt-3">Current Price Reduction</InputLabel>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Reduction</TableCell>
              <TableCell>Start Date</TableCell>
              <TableCell>End Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {itemSelected.priceReductions &&
              itemSelected.priceReductions.map((priceReduc) => (
                <TableRow>
                  <TableCell>{priceReduc.reducedPrice}</TableCell>
                  <TableCell>{priceReduc.startDate}</TableCell>
                  <TableCell>{priceReduc.endDate}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <br />
      <br />
      <div align="right">
        <Button onClick={handleAddPriceReductions} color="primary">
          Add
        </Button>
        <Button onClick={modalOpenClosePriceReduction}>Exit</Button>
      </div>
    </div>
  );


if(items !== null && items !== undefined){
  return (
    <>
      <div className="text-center">
        {checkRoleAdd()}
        <br />
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell style={{ fontWeight: "bold" }}>#ID</TableCell>
                <TableCell style={{ fontWeight: "bold" }}>CODE</TableCell>
                <TableCell style={{ fontWeight: "bold" }}>
                  DESCRIPTION
                </TableCell>
                <TableCell style={{ fontWeight: "bold" }}>
                  CREATION DATE
                </TableCell>
                <TableCell style={{ fontWeight: "bold" }}>PRICE</TableCell>
                <TableCell style={{ fontWeight: "bold" }}>STATE</TableCell>
                <TableCell style={{ fontWeight: "bold" }}>CREATOR</TableCell>
                <TableCell style={{ fontWeight: "bold" }}>ACTIONS</TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {(rowsPerPage > 0 //TODO
                ? items &&
                items.slice(
                  page * rowsPerPage,
                  page * rowsPerPage + rowsPerPage
                  )
                  : items && items
                  ).map((item) => (
                    <TableRow key={item.idItem}>
                  <TableCell>#{item.idItem}</TableCell>
                  <TableCell>{item.itemCode}</TableCell>
                  <TableCell>{item.description}</TableCell>
                  <TableCell>{item.creationDate}</TableCell>
                  <TableCell>{item.price}</TableCell>
                  <TableCell style={{ color: item.state == "ACTIVE" ? "green" : "red" }}>
                    {item.state}
                  </TableCell>
                  <TableCell>
                    {item.creator}
                  </TableCell>
                  {checkRoleActions(item)}
                </TableRow>
              ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
                  colSpan={8}
                  count={items?.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: { "aria-label": "rows per page" },
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                  />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>

        <Modal open={modalSave} onClose={modalOpenCloseSave}>
          {bodyModalSave}
        </Modal>

        <Modal open={modalEdit} onClose={modalOpenCloseSave}>
          {bodyModalEdit}
        </Modal>

        <Modal open={modalDelete} onClose={modalOpenCloseDelete}>
          {bodyModalDelete}
        </Modal>

        <Modal
          open={modalPriceReduction}
          onClose={modalOpenClosePriceReduction}
          >
          {bodyModalPriceReduction}
        </Modal>
      </div>
    </>
  );
}else{
  return null;
}
};
