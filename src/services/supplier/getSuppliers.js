import axios from "axios";
import { loadSuppliers } from "../../actions/supplierAction";
import { basicUrl } from "../../constant/basicUrl";

export const getSuppliers = async (dispatch) => {
  var token = localStorage.getItem('token');

  await axios.get(`${basicUrl}/findSuppliers`, { 'headers': { 'Authorization': `Bearer ${token}` } })
  .then((response => {
    dispatch(loadSuppliers(response.data));
  }));  
}


  