import axios from "axios";
import { loadUsers } from "../../actions/userAction";
import { basicUrl } from "../../constant/basicUrl";

export const getUsers = async (dispatch) => {
  var token = localStorage.getItem('token');

  await axios.get(`${basicUrl}/findUsers`, { 'headers': { 'Authorization': `Bearer ${token}` } })
  .then((response => {
    dispatch(loadUsers(response.data));
  }));  
}

