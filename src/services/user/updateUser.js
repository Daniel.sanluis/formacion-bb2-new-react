import swal from "sweetalert";
import { basicUrl } from "../../constant/basicUrl";
import axios from "axios";
import { actualizeItem } from "../../actions/itemAction";

export const updateUser = async (dispatch, user) => {
  var token = localStorage.getItem('token');
 
  await axios.put(`${basicUrl}/updateUser`, user,{ 'headers':
   { 'Content-Type': 'application/json',
     'Authorization': `Bearer ${token}` } })
    .then((response => {
      if(response.status === 200){
        swal("Success!", "The user has been successfully updated", "success");          
      }else{
        swal("Error!",  "The user has not been successfully updated", "error");
      }
      dispatch(actualizeItem(response.data));
    }));
}