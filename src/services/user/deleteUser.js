import swal from "sweetalert";
import axios from "axios";
import { basicUrl } from "../../constant/basicUrl";
import { removeUser } from "../../actions/userAction";

export const deleteUser = async (dispatch, id) => {

  var token = localStorage.getItem('token');

  await axios.delete(`${basicUrl}/deleteUser/${id}`, { 'headers': { 'Authorization': `Bearer ${token}` } })
  .then((response => {
    if (response.status === 200) {
      swal("Success!", "The user has been successfully removed", "success");
    }else{
      swal("Error!", "The user has not been removed", "error");
    }
    dispatch(removeUser(response.data));
  }));
}