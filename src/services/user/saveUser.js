import { basicUrl } from "../../constant/basicUrl";
import axios from "axios";
import { addUser } from "../../actions/userAction";

export const saveUser = async (dispatch, user) => {
  var token = localStorage.getItem('token');

  if(user.enabled !== "true" && user.enabled !== "false"){
    user.enabled = "true";
  }
  
  await axios.post(`${basicUrl}/saveUser`, user,{ 'headers':
   { 'Content-Type': 'application/json',
     'Authorization': `Bearer ${token}` } })
    .then((response => {
      dispatch(addUser(response.data));
    }));
}

