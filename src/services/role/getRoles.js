import axios from "axios";
import { loadRoles } from "../../actions/roleAction";
import { basicUrl } from "../../constant/basicUrl";

export const getRoles = async (dispatch) => {
  var token = localStorage.getItem('token');

  await axios.get(`${basicUrl}/findRoles`, { 'headers': { 'Authorization': `Bearer ${token}` } })
  .then((response => {
    dispatch(loadRoles(response.data));
  }));  
}

