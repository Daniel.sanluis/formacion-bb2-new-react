import swal from "sweetalert";
import axios from "axios";
import { basicUrl } from "../../constant/basicUrl";
import { removeItem } from "../../actions/itemAction";

export const deleteItem = async (dispatch, id) => {

  var token = localStorage.getItem('token');

  await axios.delete(`${basicUrl}/shop/deleteItem/${id}`, { 'headers': { 'Authorization': `Bearer ${token}` } })
  .then((response => {
    if (response.status === 200) {
      swal("Success!", "The item has been successfully removed", "success");
    }else{
      swal("Error!", "The item has not been removed", "error");
    }
    dispatch(removeItem(response.data));
  }));
}
