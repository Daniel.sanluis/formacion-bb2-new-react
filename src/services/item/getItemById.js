import { basicUrl } from "../../constant/basicUrl";
import axios from "axios";
import { loadItemById } from "../../actions/itemAction";

export const getItemById = async (dispatch, id) => {
  var token = localStorage.getItem('token');

  await axios.get(`${basicUrl}/shop/${id}`, { 'headers': { 'Authorization': `Bearer ${token}` } })
    .then((response => {
      dispatch(loadItemById(response.data));
    }));
}


