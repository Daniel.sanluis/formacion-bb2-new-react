import swal from "sweetalert";
import { basicUrl } from "../../constant/basicUrl";
import axios from "axios";
import { actualizeItem } from "../../actions/itemAction";

export const updateItem = async (dispatch, item) => {
  var token = localStorage.getItem('token');

  await axios.put(`${basicUrl}/shop/updateItem`, item,{ 'headers':
   { 'Content-Type': 'application/json',
     'Authorization': `Bearer ${token}` } })
    .then((response => {
      if(response.status === 200){
        swal("Success!", "The item has been successfully updated", "success");          
      }else{
        swal("Error!", "Price Reductions dates cannot be overlapping", "error");
      }
      dispatch(actualizeItem(response.data));
    }));
}


