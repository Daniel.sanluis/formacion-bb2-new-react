import axios from "axios";
import { loadItems } from "../../actions/itemAction";
import { basicUrl } from "../../constant/basicUrl";


export const getItems = async (dispatch) => {
  var token = localStorage.getItem('token');

  await axios.get(`${basicUrl}/shop/findItems`, { 'headers': { 'Authorization': `Bearer ${token}` } })
  .then((response => {
    dispatch(loadItems(response.data));
  }));  
}


