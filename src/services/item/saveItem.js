import { basicUrl } from "../../constant/basicUrl";
import axios from "axios";
import { addItem } from "../../actions/itemAction";

export const saveItem = async (dispatch, item) => {
  var token = localStorage.getItem('token');
  const idCreator = localStorage.getItem('idUser');
  if (item.state == "") {
    item.state = "ACTIVE";
  }

  await axios.post(`${basicUrl}/shop/saveItem/${idCreator}`, item,{ 'headers':
   { 'Content-Type': 'application/json',
     'Authorization': `Bearer ${token}` } })
    .then((response => {
      dispatch(addItem(response.data));
    }));
}
