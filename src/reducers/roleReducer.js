

import { ROLES_LOAD } from '../types/types';
import initialState from "../utils/initialState";


export const roleReducer = (state = initialState, action) => {
    switch (action.type) {
        case ROLES_LOAD:
            return {
                ...state, 
                roles: action.data
            }     
        

        default:
            return state;
    }
}
