import { USER_ADD_NEW, USER_LOAD, USER_UPDATE, USER_DELETE } from "../types/types";
import initialState from "../utils/initialState";


export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOAD:
      return {
        ...state,
        users: action.data,
      };

    case USER_ADD_NEW:
      return {
        ...state,
        users: [...state.users, action.data],
      };

    case USER_UPDATE:
      return {
        ...state,
        users: state.users.map((user) =>
          user.idUser === action.data.idUser ? action.data : user
        ),
      };

    case USER_DELETE:
      return {
        ...state,
        users: state.users.filter((user) => user.idUser !== action.data),
      };

    default:
      return state;
  }
};
