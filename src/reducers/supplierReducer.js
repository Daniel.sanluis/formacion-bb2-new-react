
import { SUPPLIER_LOAD } from '../types/types';
import initialState from "../utils/initialState";

export const supplierReducer = (state = initialState, action) => {
    switch (action.type) {
        case SUPPLIER_LOAD:
            return {
                ...state, 
                suppliers: action.data
            }     
        

        default:
            return state;
    }
}
