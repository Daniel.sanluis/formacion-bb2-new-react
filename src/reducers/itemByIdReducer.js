import { ITEM_LOAD_BY_ID } from "../types/types";

export const itemByIdReducer = (state = {}, action) => {
    switch (action.type) {
        case ITEM_LOAD_BY_ID:
    return {
        ...state, 
        item: action.data    }     

        default:
            return state;
    }
}


