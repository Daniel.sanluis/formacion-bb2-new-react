import { ITEM_ADD_NEW, ITEM_LOAD, ITEM_DELETE, ITEM_UPDATE } from "../types/types";
import initialState from "../utils/initialState";


export const itemReducer = (state = initialState, action) => {
    switch (action.type) {
        case ITEM_LOAD:    
            return {
                    ...state, 
                    items: action.data
                }           

        case ITEM_DELETE:
            return {          
                ...state,
                items: state.items.filter( item => item.idItem !== action.data )
            }

        case ITEM_ADD_NEW:     
            return{
                ...state,
                items: [...state.items, action.data ] 
            }

        case ITEM_UPDATE:
            return{
                ...state,
                items: state.items.map(
                    item => item.idItem === action.data.idItem
                        ? action.data
                        : item
                )
            }

        default:
            return state;
    }

}

