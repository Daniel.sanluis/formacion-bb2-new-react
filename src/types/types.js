
export const LOGIN = '[Auth] Login'; 
export const LOGOUT = '[Auth] Logout'; 
export const ITEM_LOAD = '[Items] Load items'; 
export const ITEM_LOAD_BY_ID = '[Items] Load item by id'; 
export const ITEM_ADD_NEW = '[Items] Add item'; 
export const ITEM_DELETE = '[Items] Delete item'; 
export const ITEM_UPDATE = '[Items] Update Item'; 
export const SUPPLIER_LOAD = '[Supliers] Load suppliers';
export const ROLES_LOAD = '[Roles] Load roles';
export const USER_LOAD = '[Users] Load users'; 
export const USER_ADD_NEW = '[Users] Add user'; 
export const USER_DELETE = '[Users] Delete user'; 
export const USER_UPDATE = '[Users] Update user'; 
