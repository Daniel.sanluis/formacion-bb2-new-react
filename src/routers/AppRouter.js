import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from 'react-router-dom';
import { LoginScreen } from '../components/auth/LoginScreen';
import { DashboardRoutes } from './DashboardRoutes';
import { PrivateRoute } from './PrivateRoute';


export const AppRouter = () => {
    return (
        <Router>
            <div>
                <Switch> 
                    <Route exact path="/login" component={ LoginScreen } />
                                       
                    <Route 
                        path="/" 
                        component={ DashboardRoutes } 
                        //isAuthenticated={localStorage.getItem('logged')}
                    />

                </Switch>
            </div>
        </Router>
    )
}
