
import { ROLES_LOAD } from "../types/types";

export const loadRoles = (data) => ({type: ROLES_LOAD, data});