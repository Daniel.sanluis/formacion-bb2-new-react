import { SUPPLIER_LOAD } from "../types/types";

export const loadSuppliers = (data) => ({type: SUPPLIER_LOAD, data});