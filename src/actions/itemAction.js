import { ITEM_ADD_NEW, ITEM_DELETE, ITEM_UPDATE, ITEM_LOAD_BY_ID, ITEM_LOAD } from "../types/types";

export const loadItems = (data) => ({type: ITEM_LOAD, data});
export const loadItemById = (data) => ({type: ITEM_LOAD_BY_ID, data});
export const removeItem = (data) => ({type: ITEM_DELETE, data});
export const addItem = (data) => ({type: ITEM_ADD_NEW, data});
export const actualizeItem = (data) => ({type: ITEM_UPDATE, data});