import { USER_ADD_NEW, USER_LOAD, USER_UPDATE, USER_DELETE } from "../types/types";

export const loadUsers = (data) => ({type: USER_LOAD, data});
export const removeUser = (data) => ({type: USER_DELETE, data});
export const addUser = (data) => ({type: USER_ADD_NEW, data});
export const actualizeUser = (data) => ({type: USER_UPDATE, data});

