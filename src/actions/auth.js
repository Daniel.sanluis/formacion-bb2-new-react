
import { basicUrl } from '../constant/basicUrl';
import { LOGIN } from '../types/types';

import axios from "axios";




export const startLogin = async (email, pass, dispatch) => {
    await axios.post(`${basicUrl}/login`, {}, {
        auth:
        {
            username: email,
            password: pass
        }
    })
        .then((response => {
            localStorage.clear();
            localStorage.setItem('token', "Bearer " + response.headers.authorization);
            login(email, dispatch);
        }));
}


export const login = async (email,dispatch) => {
    var token = localStorage.getItem('token');
    await axios.get(`${basicUrl}/user/${email}`, { 'headers': { 'Authorization': `Bearer ${token}` } })
        .then((response => {
            dispatch(logged(response.data));           
        }));
      
}      

export const logged = (user) => { 

    localStorage.setItem('idUser', user.idUser);
    localStorage.setItem('user', user.username);
    localStorage.setItem('role', JSON.stringify(user.roles));
    localStorage.setItem('logged', true);

    return {
        type: LOGIN,
        payload: {
            user
        }
    }
}
/*
export const startLogin = (username, password, dispatch) => {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

        var urlencoded = new URLSearchParams();
        urlencoded.append("user", username);
        urlencoded.append("password", password);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
          };

        fetch(`${basicUrl}/login`, requestOptions)
            .then(response => response.text())
            .then(result => {
                const user = JSON.parse(result);
                if (user.exception) {
                    swal("Error!", "You user not exist!", "error");
                } else {
                    dispatch(login(user));
                }
            })

}

export const login = (user) => {
    localStorage.clear();
    localStorage.setItem('idUser', user.idUser);
    localStorage.setItem('user', user.username);
    localStorage.setItem('role', JSON.stringify(user.roles));
    localStorage.setItem('token', "Bearer " + user.token);
    localStorage.setItem('logged', true);
    return {
        type: LOGIN,
        payload: {
            user
        }
    }
}*/


