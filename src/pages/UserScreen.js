import React from "react";
import { UserTable } from "../components/user/UserTable";

export const UserScreen = ({ history }) => {
  return (
    <>
     <br />
      <h3>User List</h3>
      <hr />
      <UserTable history={history} />
    </>
  );
};
